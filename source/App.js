import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Main, ShowPhoto } from './Pages';


const MainNavigator = createStackNavigator({
    Main:      { screen: Main },
    ShowPhoto: { screen: ShowPhoto },
});

const App = createAppContainer(MainNavigator);

export default App;
